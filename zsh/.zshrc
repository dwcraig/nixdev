export PATH=$HOME/.local/bin:$HOME/.local/share/node/bin:$PATH

if [ -e /home/dwcraig/.nix-profile/etc/profile.d/nix.sh ]; then . /home/dwcraig/.nix-profile/etc/profile.d/nix.sh; fi # added by Nix installer

export ZSH="$HOME/.oh-my-zsh"

ZSH_THEME="robbyrussell"

plugins=(direnv fd fzf git)

export LANG=en_US.UTF-8
export ANDROID_HOME=/home/dwcraig/Android/Sdk

source $ZSH/oh-my-zsh.sh

alias zshconfig="nvim ~/.zshrc"
alias vim="nvim"
alias cat="bat"
alias sail="./vendor/bin/sail"

eval "$(direnv hook zsh)"
export PATH=/home/dwcraig/Android/Sdk/platform-tools:$PATH

# Add for Homebrew
export PATH="/usr/local/sbin:$PATH"

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

