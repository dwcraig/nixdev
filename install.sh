# install nix
curl -L https://nixos.org/nix/install | sh

# source nix
. ~/.nix-profile/etc/profile.d/nix.sh

# install packages
nix-env -iA \
	nixpkgs.git \
	nixpkgs.neovim \
	nixpkgs.tmux \
    nixpkgs.tree-sitter \
    nixpkgs.nodejs \
	nixpkgs.stow \
	nixpkgs.yarn \
	nixpkgs.fzf \
    nixpkgs.fd \
	nixpkgs.ripgrep \
	nixpkgs.bat \
	nixpkgs.gnumake \
	nixpkgs.gcc \
	nixpkgs.direnv \
    nixpkgs.mc \
    nixpkgs.zip \
    nixpkgs.unzip \
	nixpkgs.zsh

# stow dotfiles
stow git
stow nvim
stow tmux
stow zsh
stow npm

# add zsh as a login shell
command -v zsh | sudo tee -a /etc/shells

# use zsh as default shell
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"

# bundle zsh plugins 
# antibody bundle < ~/.zsh_plugins.txt > ~/.zsh_plugins.sh

#install vim plug
#curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs \
#       https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

# install neovim plugins
#nvim --headless +PlugInstall +qall

# TODO: install treesitter stuff for nvim.
# TODO: install global stuff from node

# Install Brave
#sudo apt install apt-transport-https curl

#sudo curl -fsSLo /usr/share/keyrings/brave-browser-archive-keyring.gpg https://brave-browser-apt-release.s3.brave.com/brave-browser-archive-keyring.gpg

#echo "deb [signed-by=/usr/share/keyrings/brave-browser-archive-keyring.gpg arch=amd64] https://brave-browser-apt-release.s3.brave.com/ stable main"|sudo tee /etc/apt/sources.list.d/brave-browser-release.list

#sudo apt update

#sudo apt install brave-browser

# Use kitty terminal on MacOS
#[ `uname -s` = 'Darwin' ] && stow kitty
